/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (500, 400);
    
    // gain items
    gain.setSliderStyle(juce::Slider::LinearVertical);
    gain.setRange(0, 1);
    gain.addListener(this);
    addAndMakeVisible(gain);
    
    // oscillator choice items
    oscChoose.addItem("Sine", 1);
    oscChoose.addItem("Square", 2);
    oscChoose.addListener(this);
    addAndMakeVisible(oscChoose);
    

}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    gain.setBounds(0, 0, getWidth()/5.0, getHeight());
    oscChoose.setBounds(getWidth()/5.0, 0, getWidth(), 50);
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &gain) {
        sharedMemory.enter();
        audio.setGain(gain.getValue());
        sharedMemory.exit();
    }
}
void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    audio.changeOscillatorType(oscChoose.getSelectedId());
}

