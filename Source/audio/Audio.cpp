/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"
#include "math.h"


Audio::Audio()
{
    oscillator = &sinOscillator;
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    DBG("MIDI message received");
    if (message.getVelocity() > 0) {
        oscillator->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
        oscillator->setAmplitude(message.getVelocity()/127.0);
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        
        
//        phasePosition += phaseIncrement;
//        if (phasePosition > twoPi) {
//            phasePosition -= twoPi;
//        }
//        
//        sine = sin(phasePosition);
        
        *outL = oscillator->nextSample() * gain;
        *outR = oscillator->nextSample() * gain;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscillator->reset();
    
}

void Audio::audioDeviceStopped()
{

}
void Audio::setGain(float inGain)
{
    gain = inGain;
}
void Audio::changeOscillatorType(int choice)
{
    if(choice == 1)
        oscillator = &sinOscillator;
    else if (choice == 2)
        oscillator = &squareOscillator;
}
