//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Dom Brown on 20/11/2014.
//
//

#ifndef __JuceBasicAudio__SquareOscillator__
#define __JuceBasicAudio__SquareOscillator__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

class SquareOscillator  :   public Oscillator
{
public:
    virtual float renderWaveShape (const float currentPhase) override;
private:
    
};


#endif /* defined(__JuceBasicAudio__SquareOscillator__) */
