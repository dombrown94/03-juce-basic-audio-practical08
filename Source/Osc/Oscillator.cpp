//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Dom Brown on 20/11/2014.
//
//

#include "Oscillator.h"
#include <cmath>

Oscillator::Oscillator()
{
    reset();
}

Oscillator::~Oscillator()
{
    
}

void Oscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}

void Oscillator::setNote (int noteNum)
{
    setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void Oscillator::setAmplitude (float amp)
{
    amplitude = amp;
}

void Oscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
    setAmplitude (0.f);
}

void Oscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency );//just to update the phaseInc
}

float Oscillator::nextSample()
{
    float out = renderWaveShape (phase ) * amplitude ;
    phase = phase + phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}
