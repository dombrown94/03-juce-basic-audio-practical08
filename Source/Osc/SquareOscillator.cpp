//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Dom Brown on 20/11/2014.
//
//

#include "SquareOscillator.h"
#include "math.h"

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    if (currentPhase <= M_PI) {
        return 1;
    }
    else
        return -1;
}